import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { combineReducers, createStore, Reducer, AnyAction } from 'redux';
import { Provider } from 'react-redux';
// import schema from './schema.json';
import schema from './SensorControl.schema.json';
// import uischema from './uischema.json';
import { Actions, jsonformsReducer, JsonFormsState, getData } from '@jsonforms/core';
import { materialCells, materialRenderers } from '@jsonforms/material-renderers';
import RatingControl from './RatingControl';
import ratingControlTester from './ratingControlTester';
import {Sector} from 'io-sensor-control-proto-src/proto-src/SensorControl_pb';
import {SensorControlClient} from 'io-sensor-control-proto-src/proto-src/SensorControl_grpc_web_pb';

const sensorControlClient: SensorControlClient = new SensorControlClient('http://localhost:8080', null, null);

export interface CounterState {
  num: number;
}

export interface ApplicationState extends JsonFormsState {
  counterState : CounterState
}

const initState: ApplicationState = {
  jsonforms: {
    cells: materialCells,
    renderers: materialRenderers
  },
  counterState:{
    num: 0
  }
}

export default function something(state:CounterState = initState.counterState, action: AnyAction):CounterState {
  switch (action.type) {
    case 'SOMETHING':
      console.log("action.type: " + action.type);
      return { ...state, num: state.num +1};
    case 'CREATESECTOR':
      {
        console.log("action.sector:", action.sector);
        const request = action.sector;
        console.log(request);
        sensorControlClient.createSector(request, {}, function(err, response) {
            if(err){
                console.log(err.code);
                console.log(err);
            } else {
              const data = response.toObject();
              console.log("Server answered ", data)
              store.dispatch(Actions.init(data, schema));
            }        
        });
      }
  }
  return state;
}

export function grpcCallReducer(state:JsonFormsState, action: AnyAction){
  return state;
}

const rootReducer: Reducer<ApplicationState, AnyAction> = combineReducers({ jsonforms: jsonformsReducer(), counterState: something});
const store = createStore(rootReducer, initState);

store.subscribe(()=>{console.log("store changed", store.getState().counterState, getData(store.getState()))});

window.onload = function(){
  const initalSector = new Sector();
  store.dispatch({type: 'CREATESECTOR', sector: initalSector});
};

//Uncomment if and when you defined a schema and uischema
//store.dispatch(Actions.init(data, schema, uischema));

// Uncomment this line (and respective import) to register our custom renderer
store.dispatch(Actions.registerRenderer(ratingControlTester, RatingControl));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
