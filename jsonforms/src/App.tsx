import { connect } from 'react-redux';
import { JsonForms } from '@jsonforms/react';
import React from 'react';
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import Typography from "@material-ui/core/Typography";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import {getData, JsonFormsState} from '@jsonforms/core';
import logo from './logo.svg';
import './App.css';
import createStyles from "@material-ui/core/styles/createStyles";
import { Sector } from 'io-sensor-control-proto-src/proto-src/SensorControl_pb';

const styles = createStyles({
  container: {
    padding: '1em'
  },
  title: {
    textAlign: 'center',
    padding: '0.25em'
  },
  dataContent: {
    display: 'flex',
    justifyContent: 'center',
    borderRadius: '0.25em',
    backgroundColor: '#cecece',
  },
  demoform: {
    margin: 'auto'
  }
});

export interface AppProps extends WithStyles<typeof styles> {
  dataAsString: string;
  sector: Sector;
  dispatch: any;
}

class App extends React.Component<AppProps, any> {
  render() {
    const { classes, dataAsString, sector, dispatch} = this.props;
    return (
      <div>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
            <h1 className="App-title">Welcome to JSON Forms with React</h1>
            <p className="App-intro">More Forms. Less Code.</p>
          </header>
        </div>

        <Grid container justify={'center'} spacing={24} className={classes.container}>
          <Grid item sm={6}>
            <Typography
              variant={'display1'}
              className={classes.title}
            >
              Bound data
            </Typography>
            <div className={classes.dataContent}>
              <pre>{dataAsString}</pre>
            </div>
          </Grid>
          <Grid item sm={6}>
            <Typography
              variant={'display1'}
              className={classes.title}
            >
              Rendered form
            </Typography>
            <div className={classes.demoform}>
              <JsonForms/>
            </div>
            <Typography variant="button" className="save-button">
              <Button color="primary" variant="contained" onClick={() => {
                  console.log("onSaveClick"); 
                  //Dispatch Action to trigger some reducer to actually interact with grpc
                  dispatch({ type: "CREATESECTOR", sector:sector});
                  //console.log({sector});
                }}> 
                Save
              </Button>
              <Button color="default" variant="outlined" onClick={()=>{
                  console.log("onCancelClick");
              }}>
                Cancel
              </Button>
            </Typography>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state: JsonFormsState) => {
  // Map data from JsonFormState to Protobuf Messages:
  const data = getData(state);
  const sector = new Sector();
  sector.setCenterazimuth(data.centerazimuth);
  sector.setWidthazimuth(data.widthazimuth);
  sector.setStartdistance(data.startdistance);
  sector.setEnddistance(data.enddistance);
  console.log("Mapped jsonformData to Sector Message",data);

  return { 
    dataAsString: JSON.stringify(getData(state), null, 2),
    sector: sector
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    dispatch: dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));