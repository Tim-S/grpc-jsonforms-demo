FROM ubuntu:cosmic

########################################################
# Essential packages for remote debugging and login in
########################################################

# install tools to run this as a remote dev environment
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    apt-utils gcc g++ openssh-server cmake build-essential gdb gdbserver rsync curl

RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# 22 for ssh server. 7777 for gdb server.
EXPOSE 22 7777

RUN useradd -ms /bin/bash debugger
RUN echo 'debugger:pwd' | chpasswd

########################################################
# Add custom packages and development environment here
########################################################

# install protobuf
ARG PROTOBUFVERSION=3.7.1
RUN mkdir -p /tmp/protobuf \
    && curl -SL https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUFVERSION}/protobuf-all-${PROTOBUFVERSION}.tar.gz \
    | tar -xzC /tmp \
    && cd /tmp/protobuf-${PROTOBUFVERSION} \
    && ./configure \
    && make -j4 && make check && make install && ldconfig

# install grpc
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
   autoconf git libtool pkg-config libgflags-dev libgtest-dev clang libc++-dev libssl-dev

#RUN git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc \
RUN git clone -b v1.19.1 https://github.com/grpc/grpc \
    && cd grpc && git submodule update --init \
    && export CC=/usr/bin/clang \
    && export CXX=/usr/bin/clang++ \
    && EMBED_OPENSSL=false make -j4 \
    && make install

# install grpc-web
RUN curl -L https://github.com/grpc/grpc-web/releases/download/1.0.4/protoc-gen-grpc-web-1.0.4-linux-x86_64 \
    --output /usr/local/bin/protoc-gen-grpc-web \
    && chmod +x /usr/local/bin/protoc-gen-grpc-web

########################################################

CMD ["/usr/sbin/sshd", "-D"]
