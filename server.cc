#include <iostream>
#include <thread>

#include <grpc++/grpc++.h>
#include <google/protobuf/util/json_util.h>

#include "SensorControl.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

// Logic and data behind the server's behavior.
class SensorControlServiceImpl final : public io::sensor::control::SensorControl::Service{
    Status CreateSector(::grpc::ServerContext* context, const ::io::sensor::control::Sector* request, ::io::sensor::control::Sector* response) override {
        google::protobuf::util::JsonPrintOptions options;
        options.add_whitespace = true;
        options.always_print_primitive_fields = true;
        options.preserve_proto_field_names = true;

        std::string jsonString;
        google::protobuf::util::MessageToJsonString(*request, &jsonString, options);
        std::cout << "Client sent " << jsonString << std::endl;

        *response = *request;
        return Status::OK;
    }
};

void RunServer() {
    std::string server_address("0.0.0.0:50051");
    SensorControlServiceImpl service;

    ServerBuilder builder;
    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    // Finally assemble the server.
    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    // Wait for the server to shutdown. Note that some other thread must be
    // responsible for shutting down the server for this call to ever return.
    server->Wait();
}

int main(int argc, char** argv) {
    RunServer();
    return 0;
}
